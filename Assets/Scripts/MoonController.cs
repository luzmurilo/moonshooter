using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoonController : StateMachine
{
    public Rigidbody2D rigidBody;
    public float speed;
    public MoonColor moonColor;


    private void Awake() 
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    // Start is called before the first frame update
    void Start()
    {
        SetState(new AimingState(this));
    }

    private void Update() 
    {
        StartCoroutine(CurrentState.Update());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        StartCoroutine(CurrentState.Move());
    }

    private void OnCollisionEnter2D(Collision2D other) 
    {
        CurrentState.Collision(other.collider);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        CurrentState.Collision(other);
        
    }

    public void DestroySelf()
    {
        GameObject.Destroy(gameObject);
    }
}
