
public struct ConnectionData
{
    public StarController Star;
    public MoonController Moon;

    public ConnectionData(StarController star, MoonController moon)
    {
        this.Star = star;
        this.Moon = moon;
    }
}
