using System;
using UnityEngine;


[Serializable]
public struct MoonColor
{
    public string name;
    public Color color;

    public MoonColor(string name, Color color)
    {
        if (name == null)
        {
            this.name = "color";
        }
        if (color == null)
        {
            this.color = Color.black;
        }

        this.name = name;
        this.color = color;
    }
}
