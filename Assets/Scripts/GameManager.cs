using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    [SerializeField] List<MoonColor> Colors;
    public List<StarController> Stars {get; private set;}
    private bool levelComplete;
    public bool gamePaused;
    public UnityEvent OnLevelComplete;
    public UnityEvent OnLevelLost;
    public UnityEvent OnLevelPaused;
    public UnityEvent OnLevelUnpaused;

    private void Awake() 
    {
        if (Instance == null)
        {
            Instance = this;
        }
        Stars = new List<StarController>();
        levelComplete = false;
        gamePaused = false;
    }

    private void Update() 
    {
        if (!levelComplete)
        {
            levelComplete = CheckLevelStatus();
            if (levelComplete)
                HandleLevelComplete();
        }
    }

    private bool CheckLevelStatus()
    {
        bool levelStatus = true;
        Stars.ForEach(delegate(StarController star)
        {
            if (!star.isComplete)
                levelStatus = false;
        });
        
        return levelStatus;
    }

    private void HandleLevelComplete()
    {
        gamePaused = true;
        OnLevelComplete.Invoke();
    }

    public void HandleLevelLost()
    {
        gamePaused = true;
        OnLevelLost.Invoke();
    }

    public void HandleLevelPaused()
    {
        gamePaused = true;
        OnLevelPaused.Invoke();
    }

    public void HandleLevelUnpaused()
    {
        gamePaused = false;
        OnLevelUnpaused.Invoke();
    }

    public MoonColor GetRandomColor()
    {
        int index = Random.Range(0, Colors.Count);
        MoonColor randomColor = Colors[index];
        return randomColor;
    }

    public int AddStar(StarController star)
    {
        if (star == null)
            return -1;
        

        Stars.Add(star);
        return Stars.Count - 1;
    }
    public bool RemoveStar(int starIndex)
    {
        if (starIndex >= Stars.Count)
        {
            Debug.LogWarning("Failed to remove star!");
            return false;
        }
        else
        {
            Stars.RemoveAt(starIndex);
            return true;
        }
    }
    public bool RemoveStar(StarController starToRemove)
    {
        if (!Stars.Exists(item => item == starToRemove))
        {
            Debug.LogWarning("Failed to remove star!");
            return false;
        }
        else
        {
            Stars.Remove(starToRemove);
            return true;
        }
    }

    public void HandleMoonConnection(ConnectionData connection)
    {
        SFXPlayer.Instance.PlayHitSound();
        // Look for a Star that has a Moon of the same color
        foreach(StarController star in Stars)
        {
            if ((star != connection.Star) && (star.Moons.Exists(moon => moon.moonColor.name == connection.Moon.moonColor.name)))
            {
                // Test if the stars already have a connection
                if (!star.Connections.Exists(i => i == connection.Star.Index))
                {
                    ConnectStars(connection.Star, star, connection.Moon.moonColor);
                    return;
                }
            }
        }
        // Check if this moon was the last on the pool
        if (MoonPool.Instance.isOutOfMoons)
        {
            HandleLevelLost();
        }
    }

    private void ConnectStars(StarController star1, StarController star2, MoonColor moonColor)
    {
        SFXPlayer.Instance.PlayConnectionSound();
        star1.DestroyMoon(moonColor);
        star2.DestroyMoon(moonColor);
        star1.DrawLineToTarget(star2.transform.position, moonColor);
        star1.Connections.Add(star2.Index);
        star2.Connections.Add(star1.Index);
    }
}
