using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MoonPool : MonoBehaviour
{
    public static MoonPool Instance;
    public int maxMoons;
    [SerializeField] GameObject moonPrefab;
    private int moonCount;
    public bool isOutOfMoons {get; private set;}
    public UnityEvent<int> OnLoadNewMoon;

    private void Awake() {
        if (Instance == null)
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        moonCount = 0;
        isOutOfMoons = false;
        if (transform.childCount == 0)
        {
            LoadMoon();
        }
        else
        {
            moonCount = transform.childCount;
        }
    }

    public void LoadMoon()
    {
        if (moonCount < maxMoons)
        {
            StartCoroutine(LoadMoonCoroutine());
            moonCount++;
            OnLoadNewMoon.Invoke(moonCount - 1);
        }
        else
        {
            OnLoadNewMoon.Invoke(moonCount);
            isOutOfMoons = true;
        }
    }

    private IEnumerator LoadMoonCoroutine()
    {
        yield return new WaitForSeconds(1.0f);
        Instantiate(moonPrefab, transform.position, transform.rotation, transform);
    }
}
