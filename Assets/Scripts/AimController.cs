using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimController : MonoBehaviour
{
    public static AimController Instance;
    Camera mainCamera;
    public Vector3 lookDirection {get; private set;}
    public bool validMousePosition;

    private void Awake() 
    {
        if (Instance == null)
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if(!GameManager.Instance.gamePaused)
            LookAtMouse();
    }

    void LookAtMouse()
    {
        Vector3 mouseScreenPosition = Input.mousePosition;
        if (mouseScreenPosition.x > 840.0f && mouseScreenPosition.y > 540.0f)
        {
            validMousePosition = false;
        }
        else
        {
            validMousePosition = true;
            Vector3 position = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            position.z = 0f;
            lookDirection = (position - transform.position).normalized;
            float angle = Mathf.Atan2(lookDirection.x, lookDirection.y) * Mathf.Rad2Deg;
            transform.eulerAngles = new Vector3(0, 0, -angle);
        }
    }
}
