using UnityEngine;

public class SFXPlayer : MonoBehaviour
{
    public static SFXPlayer Instance;
    private AudioSource audioSource;
    [SerializeField] AudioClip buttonSound;
    [SerializeField] AudioClip startSound;
    [SerializeField] AudioClip winSound;
    [SerializeField] AudioClip loseSound;
    [SerializeField] AudioClip connectionSound;
    [SerializeField] AudioClip moonHitSound;

    private void Awake() 
    {
        if (SFXPlayer.Instance == null)
        {
            SFXPlayer.Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        audioSource = GetComponent<AudioSource>();
    }

    public void PlayButtonSound()
    {
        audioSource.PlayOneShot(buttonSound);
    }
    public void PlayWinSound()
    {
        audioSource.PlayOneShot(winSound);
    }
    public void PlayStartSound()
    {
        audioSource.PlayOneShot(startSound);
    }
    public void PlayLoseSound()
    {
        audioSource.PlayOneShot(loseSound);
    }
    public void PlayConnectionSound()
    {
        audioSource.PlayOneShot(connectionSound);
    }
    public void PlayHitSound()
    {
        audioSource.PlayOneShot(moonHitSound);
    }
}