using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField] GameObject buttons;
    [SerializeField] GameObject levels;

    public void StartGameButton()
    {
        SFXPlayer.Instance.PlayStartSound();
        LevelManager.Instance.LoadNewScene(1);
    }

    public void LevelSelectButton()
    {
        buttons.SetActive(false);
        levels.SetActive(true);
    }

    public void StartSelectedLevel(int levelIndex)
    {
        LevelManager.Instance.LoadNewScene(levelIndex);
    }

    public void ExitButton()
    {
        //SFXPlayer.Instance.PlayButtonSound();
        LevelManager.Instance.Exit();
    }
}
