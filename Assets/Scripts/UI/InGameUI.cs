using UnityEngine;
using TMPro;

public class InGameUI : MonoBehaviour
{
    [SerializeField] GameObject winScreen;
    [SerializeField] GameObject loseScreen;
    [SerializeField] GameObject skyScreen;
    [SerializeField] GameObject sideButtons;
    [SerializeField] TextMeshProUGUI shotsCounter;

    private void Start() 
    {
        UpdateShotsCounter(0);
    }

    public void RestartButton()
    {
        SFXPlayer.Instance.PlayButtonSound();
        LevelManager.Instance.RestartScene();
    }

    public void ExitButton()
    {
        SFXPlayer.Instance.PlayButtonSound();
        LevelManager.Instance.LoadNewScene(0);
    }

    public void NextButton()
    {
        SFXPlayer.Instance.PlayStartSound();
        LevelManager.Instance.NextScene();
    }

    public void SkyButton()
    {
        SFXPlayer.Instance.PlayButtonSound();
        skyScreen.SetActive(true);
        GameManager.Instance.HandleLevelPaused();
    }

    public void CloseSkyButton()
    {
        SFXPlayer.Instance.PlayButtonSound();
        skyScreen.SetActive(false);
        GameManager.Instance.HandleLevelUnpaused();
    }

    public void ActivateWinScreen()
    {
        winScreen.SetActive(true);
        sideButtons.SetActive(false);
        SFXPlayer.Instance.PlayWinSound();
    }

    public void ActivateLoseScreen()
    {
        SFXPlayer.Instance.PlayLoseSound();
        loseScreen.SetActive(true);
        sideButtons.SetActive(false);
    } 

    public void UpdateShotsCounter(int number)
    {
        shotsCounter.text = "" + number + " / " + MoonPool.Instance.maxMoons;
    }
}
