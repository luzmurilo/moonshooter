using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line : MonoBehaviour
{
    public LineRenderer lineRenderer;

    private void Awake() 
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void SetLinePoints(Vector3 point1, Vector3 point2)
    {
        if (lineRenderer == null)
           lineRenderer = GetComponent<LineRenderer>();
         
        lineRenderer.positionCount = 2;
        Vector3[] points = {point1, point2};
        lineRenderer.SetPositions(points);
    }

    public void SetLineColors(Color startColor, Color endColor)
    {
        lineRenderer.startColor = startColor;
        lineRenderer.endColor = endColor;
    }
}
