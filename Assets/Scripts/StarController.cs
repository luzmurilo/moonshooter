using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StarController : MonoBehaviour
{
    [SerializeField] private int index = -1;
    public int Index
    {
        get
        {
            return index;
        }
        set
        {
            if (index == -1)
                index = value;
            // else
            //     Debug.LogWarning("Trying to overwrite star index!");
        }
    }

    public List<int> Connections;
    [SerializeField] private List<int> goodConnections;
    public bool isComplete {get; private set;}
    private List<MoonController> moonsConnected;
    public List<MoonController> Moons
    {
        get
        {
            return moonsConnected;
        }
    }
    private MoonColor color;
    public MoonColor Color
    {
        get
        {
            return color;
        }
        set
        {
            ChangeColor(value);
        }
    }

    public UnityEvent<ConnectionData> OnMoonConnect;
    [SerializeField] GameObject linePrefab;
    

    private void OnEnable() 
    {
        Index = GameManager.Instance.AddStar(this);
        OnMoonConnect.AddListener(GameManager.Instance.HandleMoonConnection);
        isComplete = false;
    }
    private void OnDisable() 
    {
        GameManager.Instance.RemoveStar(this);
        OnMoonConnect.RemoveAllListeners();

    }

    private void Awake() 
    {
        moonsConnected = new List<MoonController>();
        color = new MoonColor();
    }

    private void Update() 
    {
        if (!GameManager.Instance.gamePaused)
        {
            if (Connections.TrueForAll(i => goodConnections.Contains(i)))
            {
                if (Connections.Count == goodConnections.Count)
                    isComplete = true;
            }
            else
            {
                GameManager.Instance.HandleLevelLost();
            }
        }
    }

    public void LoadNewMoon(MoonController newMoon)
    {
        if (newMoon == null)
            return;
        
        ConnectionData conn = new ConnectionData(this, newMoon);
        moonsConnected.Add(newMoon);

        OnMoonConnect.Invoke(conn);

        ChangeColor(MixColors(newMoon.moonColor, color));
    }

    public void DestroyMoon(MoonController moon)
    {
        if (!Moons.Exists(m => moon))
        {
            Debug.LogWarning("Can't find Moon to destroy!");
            return;
        }

        Moons.Remove(moon);
        moon.DestroySelf();
    }
    public void DestroyMoon(MoonColor moonColor)
    {
        MoonController moon = Moons.Find(m => m.moonColor.name == moonColor.name);
        if (moon == null)
        {
            Debug.LogWarning("Can't find Moon to destroy!");
            return;
        }
        Moons.Remove(moon);
        moon.DestroySelf();
    }

    public MoonColor MixColors(MoonColor color1, MoonColor color2)
    {
        MoonColor mixedColor = new MoonColor();
        mixedColor.color.a = 1.0f;
        mixedColor.color.g = Mathf.Max(color1.color.g, color2.color.g);
        mixedColor.color.b = Mathf.Max(color1.color.b, color2.color.b);
        mixedColor.color.r = Mathf.Max(color1.color.r, color2.color.r);

        if (color1.name != null && color2.name != null)
            mixedColor.name = color1.name + " + " + color2.name;
        else if (color1.name != null)
            mixedColor.name = color1.name;
        else if (color2.name != null)
            mixedColor.name = color2.name;
        else
            mixedColor.name = "undefined";

        return mixedColor;
    }

    private void ChangeColor(Color newColor)
    {
        color.color = newColor;
        color.name = "new color";
        GetComponent<SpriteRenderer>().color = color.color;
    }
    private void ChangeColor(MoonColor newColor)
    {
        color = newColor;
        GetComponent<SpriteRenderer>().color = color.color;
    }

    public void DrawLineToTarget(Vector3 target, MoonColor moonColor)
    {
        GameObject lineInstance = Instantiate<GameObject>(linePrefab, transform.position, transform.rotation, transform);
        Line lineController = lineInstance.GetComponent<Line>();
        lineController.SetLinePoints(transform.position, target);
        lineController.SetLineColors(moonColor.color, moonColor.color);
    }
}
