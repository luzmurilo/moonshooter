using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingState : State
{
    protected Vector2 velocity;
    public MovingState(MoonController cont, Vector3 vel) : base(cont)
    {
        velocity = vel;
    }

    public override IEnumerator Start()
    {
        MoonPool.Instance.LoadMoon();
        Controller.rigidBody.velocity = velocity;
        return base.Start();
    }
    
    public override IEnumerator Collision(Collider2D other)
    {
        if (other.CompareTag("Planet"))
        {
            Controller.SetState(new OrbitState(Controller, other));
        }
        return base.Collision(other);
    }

    public override IEnumerator Update()
    {
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(Controller.transform.position);
        CheckOutOfBounds(screenPosition, Camera.main);
        return base.Update();
    }

    private void CheckOutOfBounds(Vector3 screenPosition, Camera cam)
    {
        if (screenPosition.x < 0 || screenPosition.y < 0 || screenPosition.x > cam.pixelWidth || screenPosition.y > cam.pixelHeight)
        {
            // If it's the last moon, game over
            if (MoonPool.Instance.isOutOfMoons)
                GameManager.Instance.HandleLevelLost();
                
            Controller.DestroySelf();
        }
    }
}
