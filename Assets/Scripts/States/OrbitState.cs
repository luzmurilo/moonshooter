using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitState : State
{
    public Transform planet;

    public OrbitState(MoonController cont, Collider2D other) : base(cont)
    {
        planet = other.transform;
    }

    public override IEnumerator Start()
    {
        Controller.rigidBody.velocity = Vector2.zero;
        Controller.rigidBody.bodyType = RigidbodyType2D.Static;
        planet.gameObject.GetComponent<StarController>().LoadNewMoon(Controller);
        return base.Start();
    }

    public override IEnumerator Move()
    {
        Controller.transform.Rotate(0, 0, -2);
        Controller.transform.RotateAround(planet.position, Vector3.forward, 1f);
        return base.Move();
    }
}
