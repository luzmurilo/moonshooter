using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State
{
    protected MoonController Controller;

    public State(MoonController cont)
    {
        Controller = cont;
    }

    public virtual IEnumerator Start()
    {
        yield break;
    }

    public virtual IEnumerator Move()
    {
        yield break;
    }

    public virtual IEnumerator Update()
    {
        yield break;
    }

    public virtual IEnumerator Collision(Collider2D other)
    {
        yield break;
    }
}
