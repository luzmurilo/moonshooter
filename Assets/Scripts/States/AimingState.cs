using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimingState : State
{
    public AimingState(MoonController cont) : base(cont)
    {

    }

    public override IEnumerator Start()
    {
        Controller.moonColor = GameManager.Instance.GetRandomColor();
        Controller.GetComponent<SpriteRenderer>().color = Controller.moonColor.color;
        return base.Start();
    }

    public override IEnumerator Move()
    {
        return base.Move();
    }

    public override IEnumerator Update()
    {
        if (Input.GetButtonDown("Fire1") && !GameManager.Instance.gamePaused && AimController.Instance.validMousePosition)
        {
            Vector3 direction = AimController.Instance.lookDirection;
            Controller.SetState(new MovingState(Controller, direction * Controller.speed));
        }
        return base.Update();
    }
}
